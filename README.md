# Varejonline - Treinamento

Criar uma loja virtual onde um cliente pode escolher produtos, adicionar ao carrinho, selecionar a forma de pagamento e finalizar o pedido.

**Abaixo estão descritas as regras de negócio e alguns requisitos obrigatórios do projeto.**

## REQUISITOS

## Clientes

Criar uma área de cadastro de clientes contendo as seguintes informações:

 * Nome
 * Login
 * Senha
 * E-mail
 * Data de nascimento
 * CPF
 * Telefones - mais de um telefone por cliente
 * Endereços - mais de um endereço por cliente (residencial, entrega, trabalho)
	* Pelo menos um endereço é de preenchimento obrigatório

Criar área para edição dos dados do cliente e exibição todos os pedidos realizados pelo cliente.

## Login

Criar tela de autenticação exibindo apenas dois campos: login e senha.
Validar os dados, caso inválido exibir erro em tela. Possibilitar acesso à página de cadastro de cliente pela tela de login.
O cliente logado pode ser salvo apenas no escopo do projeto `frontend`, enviando sempre que for preciso ao `backend`.

## Produtos/Preços

Existem produtos cadastrados na base de dados, será apenas necessário configurar os preços:

* Cada produto pode ter, no máximo, dois preços de venda. Caso possua dois preços, um deles deve ser obrigatoriamente promocional.
* Criar script de inserção de preços e executar na base de dados, não é necessário implementar na aplicação este recurso. (**Manter o script salvo** - *nome do arquivo **precos.sql***)
* No mínimo 20 produtos devem possuir preço promocional.

Criar tela para exibir os produtos ao cliente. Deve ser a tela inicial da aplicação, primeira tela que o cliente deve ver. Por padrão listar os primeiros 50 produtos.
Caso o produto possua preço promocional exibir de alguma forma o valor padrão e o valor em promoção ou percentual de desconto.
Possibilitar pesquisa de produtos por nome, código, código de barras ou produtos que estejam entre dois valores preenchidos pelo cliente.
Possibilitar exibir apenas produtos em promoção.
Possibilitar limpar filtros, voltando a exibir o padrão.
Permitir adicionar produtos ao carrinho, por padrão sempre uma quantidade será adicionada.

## Carrinho

O cliente pode adicionar produtos ao carrinho mas pode continuar escolhendo produtos. Ao sair e voltar para a aplicação, os produtos adicionados anteriormente devem ser recuperados para possibilitar a finalização do pedido.
Permitir remover produto do carrinho.
Permitir alterar a quantidade de cada produto no carrinho.

* Se a quantidade informada for 0, remover o produto do carrinho.

Identificar de alguma forma quando o carrinho está vazio e quando possui itens.
Esvaziar o carrinho se o pedido for finalizado ou o cliente remover todos os itens manualmente.

## Pedido

Ao iniciar o processo para finalizar o pedido deve ser exibido todos os produtos, quantidades, subtotal e total ao cliente.
Possibilitar a escolha de forma de pagamento: Dinheiro, Cartão, Boleto.

* Ao escolher boleto, identificar em tela e conceder 10% de desconto no total do pedido. Salvar valor sem desconto e valor com desconto.
* Não é obrigatório possibilidade de parcelamento no cartão.

Ao finalizar, exibir um resumo do pedido e possibilitar o cliente retornar à página inicial da aplicação.

* Exibir o número do pedido, podendo ser o sequencial da tabela.

## Criação de scripts - banco de dados

Realizar as operações descritas abaixo em banco de dados, salvando o comando executado em arquivo conforme descrito:

1 - Criar script para retornar todos os pedidos realizados nos últimos 30 dias, exibindo: *(nome do arquivo **pedidos.sql**)*

* Nome do cliente
* Documento do cliente
* Número do pedido
* Quantidade de itens no pedido
* Valor total do pedido
* Valor de desconto

2 - Criar script para retornar todos os produtos vendidos, exibindo: *(nome do arquivo **produtos.sql**)*

* Código do produto
* Código de barras
* Nome do produto
* Preço
* Preço promocional
* Soma de quantidades em pedidos

3 - Criar script para atualizar o preço para `R$ 55,00` de todos os produtos que contenham a palavra `GAME` em qualquer parte do nome e **não** possuam preço promocional. *(nome do arquivo **preco_produto.sql**)*

4 - Criar script para remover todos os produtos que contenham a palavra `LEGO` em qualquer parte do nome. *(nome do arquivo **remover_produtos.sql**)*

5 - Criar script para retornar o e-mail de todos os clientes que possuem itens no carrinho. *(nome do arquivo **cliente_carrinho.sql**)*

### Criação de scripts - Resultado
Adicionar os **6** arquivos criados na pasta `varejonline-treinamento/treinamento-backend/sql` e dar commit.


## MODELAGEM

## Banco de dados

As tabelas necessárias estão criadas, porém contendo apenas a coluna `id`. As demais colunas necessárias para atender os requisitos do projeto deverão ser criadas de acordo com necessidade.

Apenas a tabela `Produto` contém dados.

### Modelagem do banco de dados

![Modelagem banco de dados](https://drive.google.com/file/d/1aCcmmKMyGxLkdpBO28Djw2zyMARwRRvb/view?usp=sharing)


## Aplicação | treinamento-backend

Projeto responsável por conter as regras de negócio, acesso ao banco de dados e disponibilizar APIs para realizar todas as operações entre frontend e backend.

O domínio de `Produtos` está implementado e pode ser consultado como guia para criação das demais classes.

Deve ser respeitado a hierarquia de pacotes conforme imagem:


![Modelagem pacotes](https://drive.google.com/file/d/1-qpqsfvJW1RhCaAkToycnfpICCT9v9cH/view?usp=sharing)



## Dicas

Por padrão, utilizamos `sequences` para gerar os identificadores únicos das tabelas. Todos os `sequences` estão criados e podem ser vistos na imagem da modelagem do banco de dados.

Para utilizar este padrão, ao criar a entidade (mapeamento da tabela em classes java utilizando Hibernate) é necessário incluir algumas anotações, conforme descrito abaixo:

Java (entidade):

```javascript
@SequenceGenerator(name = "[NOME_SEQUENCE]", sequenceName = "[NOME_SEQUENCE]", allocationSize = 1)
public class Pedido {

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "[NOME_SEQUENCE]")
	private Long id;
	
}
```

Gerar um número entre 0.01 e 500 no Oracle:

```javascript
DBMS_RANDOM.VALUE(0.01,500)
```

Arredondar um número no Oracle:

```javascript
ROUND([numero],[casas])
```

Dependências adicionadas e disponíveis para utilização no projeto `treinamento-frontend`:
*(jquery está adicionado apenas por ser requisito do bootstrap e **não deve ser utilizado** para manipulação do DOM, utilizar apenas Angular para esta situação)*

[Bootstrap](https://getbootstrap.com/docs/4.0/getting-started/introduction/)


[Fontawesome](https://fontawesome.com/icons)
