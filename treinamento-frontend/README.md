# Varejonline - Treinamento

Este projeto é responsável pela apresentação do software, não deve conter regras de negócio.

## Iniciar servidor de desenvolvimento

Execute `npm start` para iniciar o servidor de desenvolvimento. Navege para `http://localhost:4200/`.

## Comunicação com APIs - backend

É necessário apenas utilizar o prefixo `/treinamento/[api-path]` ao efetuar as requisições (get, post, put, delete). As requisições serão automaticamente redirecionadas para o servidor backend, que deve estar em execução (o redirecionamento ocorre devido à configuração no arquivo `proxy-conf.json`).