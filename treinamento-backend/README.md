# Varejonline - Treinamento

Este projeto é responsável pelas regras de negócio, acesso ao banco de dados e exposição das informações através de APIs.

A criação de classes deve ser realizada utilizando os pacotes existentes e respeitar o domínio de cada pacote. Utilizar injeção de dependência para consumir qualquer recurso criado no projeto.

## Iniciar servidor de desenvolvimento

Clicar com botão direito do mouse no projeto `treinamento-backend` e ir em: `Run as` ou `Debug as` e depois em `Mavel build...`. Na janela `Edit configuration` em `Goals` informar `spring-boot:run` para iniciar o servidor de desenvolvimento.
