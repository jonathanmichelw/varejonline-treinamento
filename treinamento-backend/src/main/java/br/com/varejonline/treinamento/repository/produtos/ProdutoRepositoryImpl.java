package br.com.varejonline.treinamento.repository.produtos;

import java.util.List;

import org.hibernate.Criteria;
import org.springframework.stereotype.Repository;

import br.com.varejonline.treinamento.model.produtos.Produto;
import br.com.varejonline.treinamento.repository.RepositoryBaseImpl;

@Repository
public class ProdutoRepositoryImpl extends RepositoryBaseImpl<Produto> implements ProdutoRepositoryCustom {

	@Override
	public List<Produto> findAll() {

		Criteria criteria = super.createCriteria(Produto.class);

		@SuppressWarnings({ "unchecked" })
		List<Produto> produtos = criteria.list();
		return produtos;

	}

}
