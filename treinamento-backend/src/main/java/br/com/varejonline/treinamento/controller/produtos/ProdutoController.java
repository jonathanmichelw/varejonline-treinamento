package br.com.varejonline.treinamento.controller.produtos;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.varejonline.treinamento.dto.produtos.ProdutoDTO;
import br.com.varejonline.treinamento.service.produtos.ProdutoService;

@RestController
@RequestMapping(value = "/api/produtos")
public class ProdutoController {

	@Autowired
	private ProdutoService service;

	@GetMapping
	public List<ProdutoDTO> findAll() {

		return this.service.findAll();

	}

}
