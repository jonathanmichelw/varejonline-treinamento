package br.com.varejonline.treinamento.repository.produtos;

import java.util.List;

import br.com.varejonline.treinamento.model.produtos.Produto;

public interface ProdutoRepositoryCustom {

	List<Produto> findAll();

}
