package br.com.varejonline.treinamento.repository.produtos;

import br.com.varejonline.treinamento.model.produtos.Produto;
import br.com.varejonline.treinamento.repository.RepositoryBase;

public interface ProdutoRepository extends RepositoryBase<Produto>, ProdutoRepositoryCustom {

}
