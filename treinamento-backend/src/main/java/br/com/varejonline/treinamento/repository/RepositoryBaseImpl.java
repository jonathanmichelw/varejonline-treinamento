package br.com.varejonline.treinamento.repository;

import javax.persistence.EntityManager;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.springframework.beans.factory.annotation.Autowired;

import br.com.varejonline.treinamento.model.EntityBase;

public abstract class RepositoryBaseImpl<TEntity extends EntityBase> {

	@Autowired
	private EntityManager entityManager;

	public final Session session() {

		return entityManager.unwrap(Session.class);
	}

	@SuppressWarnings("deprecation")
	public final Criteria createCriteria(Class<?> entityType) {

		return this.session().createCriteria(entityType);

	}

	// public final TEntity save(TEntity entity) {
	//
	// this.session().save(entity);
	// return entity;
	//
	// }
	//
	// public final TEntity update(TEntity entity) {
	//
	// this.session().update(entity);
	// return entity;
	//
	// }
	//
	// public final TEntity merge(TEntity entity) {
	//
	// @SuppressWarnings("unchecked")
	// TEntity e = (TEntity) this.session().merge(entity);
	// return e;
	//
	// }
	//
	// public final void delete(TEntity entity) {
	//
	// this.session().delete(entity);
	//
	// }

}
