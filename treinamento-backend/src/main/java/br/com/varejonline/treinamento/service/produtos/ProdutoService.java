package br.com.varejonline.treinamento.service.produtos;

import java.util.List;

import br.com.varejonline.treinamento.dto.produtos.ProdutoDTO;

public interface ProdutoService {

	List<ProdutoDTO> findAll();

}
