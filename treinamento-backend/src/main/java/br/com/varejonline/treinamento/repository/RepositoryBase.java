package br.com.varejonline.treinamento.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.varejonline.treinamento.model.EntityBase;

public interface RepositoryBase<TEntity extends EntityBase> extends JpaRepository<TEntity, Long> {

}
