package br.com.varejonline.treinamento.dto.produtos;

import br.com.varejonline.treinamento.dto.DTOBase;

public class ProdutoDTO extends DTOBase {

	private String codigo;

	private String nome;

	private String codigoBarras;

	public String getCodigo() {

		return codigo;
	}

	public void setCodigo(String codigo) {

		this.codigo = codigo;
	}

	public String getNome() {

		return nome;
	}

	public void setNome(String nome) {

		this.nome = nome;
	}

	public String getCodigoBarras() {

		return codigoBarras;
	}

	public void setCodigoBarras(String codigoBarras) {

		this.codigoBarras = codigoBarras;
	}

}
