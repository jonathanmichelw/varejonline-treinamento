package br.com.varejonline.treinamento.model.produtos;

import javax.persistence.Entity;

import org.hibernate.annotations.DynamicUpdate;

import br.com.varejonline.treinamento.model.EntityBase;

@DynamicUpdate
@Entity(name = "produto")
public class Produto extends EntityBase {

	private static final long serialVersionUID = 6252204071522409083L;

	private Long id;

	private String codigo;

	private String nome;

	private String codigoBarras;

	@Override
	public Long getId() {

		return this.id;
	}

	@Override
	public void setId(Long id) {

		this.id = id;

	}

	public String getCodigo() {

		return codigo;
	}

	public void setCodigo(String codigo) {

		this.codigo = codigo;
	}

	public String getNome() {

		return nome;
	}

	public void setNome(String nome) {

		this.nome = nome;
	}

	public String getCodigoBarras() {

		return codigoBarras;
	}

	public void setCodigoBarras(String codigoBarras) {

		this.codigoBarras = codigoBarras;
	}

}
