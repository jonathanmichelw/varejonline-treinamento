package br.com.varejonline.treinamento.service.produtos;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.varejonline.treinamento.dto.produtos.ProdutoDTO;
import br.com.varejonline.treinamento.model.produtos.Produto;
import br.com.varejonline.treinamento.repository.produtos.ProdutoRepository;

@Service
@Transactional
public class ProdutoServiceImpl implements ProdutoService {

	@Autowired
	private ProdutoRepository repository;

	@Override
	public List<ProdutoDTO> findAll() {

		List<Produto> entities = this.repository.findAll();

		List<ProdutoDTO> dtos = entities.stream().map(e -> {
			ProdutoDTO dto = new ProdutoDTO();
			dto.setId(e.getId());
			dto.setCodigo(e.getCodigo());
			dto.setNome(e.getNome());
			dto.setCodigoBarras(e.getCodigoBarras());

			return dto;
		}).collect(Collectors.toList());

		return dtos;

	}

}
